package figures;

import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class EcouteurFenetre extends JPanel implements WindowListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7026643025648261516L;
	public static JFrame f;
	public static DessinFigures d;
	public EcouteurFenetre(JFrame f,DessinFigures d){
		EcouteurFenetre.f = f;
		EcouteurFenetre.d = d;
	}
	
	@Override
	public void windowActivated(WindowEvent arg0) {
		this.d.reprendreTimer();/* au debut dans DessinFigure this.anime est false donc le timer n'est pas execute*/
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		
		
	}


	public void windowClosing(WindowEvent e) {
		/*int reponse = JOptionPane.showConfirmDialog(this, "Etes-vous s�r de vouloir quitter ?", "confirmation", JOptionPane.YES_NO_CANCEL_OPTION);
		if (reponse == JOptionPane.YES_OPTION){
			EcouteurFenetre.d.stopperTimer();
			System.exit(0);
		}
		f.setSize(new Dimension(900,700));
		EcouteurFenetre.f.setVisible(true); ne fonctionne ni sous windows ni sous unix
		car la fenetre se ferme meme si on e quitte pas*/
		System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		this.d.suspendreTimer();
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub		
	}
	
	
}
