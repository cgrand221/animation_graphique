package figures;

import java.util.Vector;


/* les figures ne doivent se toucher que sur le bord si ce n'est pas le cas 
 * on ajoute au vecteur interdiction les figures pour lequelles on interdit l'interaction*/

public class trajectoires {
	Vector v;
	double gravitation;
	public trajectoires(Vector vz,boolean interaction,double gr){
		this.v = vz;
		int i=0;
		int j=0;
		this.gravitation = gr;
		Figure figAti;
		Figure figAtj;
		while (i<v.size()){
			j=i+1;/*ne pas faire l'intersection de la figure avec elle meme*/
			figAti = (Figure) this.v.elementAt(i);
			int longu = figAti.longu;
			int largu = figAti.largu;

			if (figAti.x < 0){
				if (this.gravitation != 0)
					figAti.xvitesse = -figAti.xvitesse*0.9;
				else figAti.xvitesse = -figAti.xvitesse;
				figAti.x = 0;
			}
			if (figAti.x + longu > 700){
				if (this.gravitation != 0)
					figAti.xvitesse = -figAti.xvitesse*0.9;
				else figAti.xvitesse = -figAti.xvitesse;
				figAti.x = 700 - longu;
			}
			if 	(figAti.y < 0){
				if (this.gravitation != 0)
					figAti.yvitesse = -figAti.yvitesse*0.9;
				else figAti.yvitesse = -figAti.yvitesse;
				figAti.y = 0;
			}
			if 	(figAti.y + largu > 500){
				if (this.gravitation != 0)
					figAti.yvitesse = -figAti.yvitesse*0.9;
				else figAti.yvitesse = -figAti.yvitesse;
				figAti.y = 500-figAti.largu;
			}
			if (interaction)
				while (j<v.size()){/* a interagi avec les autres*/
					figAtj = (Figure) this.v.elementAt(j);			
					if (intersection(figAti,figAtj)){
						double v = figAti.xvitesse;
						figAti.xvitesse = figAtj.xvitesse;
						figAtj.xvitesse = v;
						v = figAti.yvitesse;
						figAti.yvitesse = figAtj.yvitesse;
						figAtj.yvitesse = v;					
					}
					j++;				
				}
			i++;
		}
	}
	public Vector getV() {
		return v;
	}
	public boolean intersection(Figure a,Figure b){
		/*
		 * resultat = false => pas d'intersection
		 * resultat = true => intersection
		 */

		boolean resultat=false;
		if (a.type == 1 && b.type == 1){
			Rectangle c = (Rectangle) a;
			Rectangle d = (Rectangle) b;
			int xe = (int) c.x;
			int ye = (int) c.y;

			int xf = (int) c.x + c.getLargeur();
			int yf = (int) c.y;

			int xg = (int) c.x;
			int yg = (int) c.y + c.getHauteur();

			int xh = (int) c.x + c.getLargeur();
			int yh = (int) c.y + c.getHauteur();

			resultat = (d.dedans(xe,ye) || d.dedans(xf,yf) || d.dedans(xg,yg) || d.dedans(xh,yh));
			xe = (int) d.x;
			ye = (int) d.y;

			xf = (int) d.x + d.getLargeur();
			yf = (int) d.y;

			xg = (int) d.x;
			yg = (int) d.y + d.getHauteur();

			xh = (int) d.x + d.getLargeur();
			yh = (int) d.y + d.getHauteur();
			resultat = (resultat || c.dedans(xe,ye) || c.dedans(xf,yf) || c.dedans(xg,yg) || c.dedans(xh,yh));
		}

		if (a.type == 2 && b.type == 2){
			Cercle c = (Cercle) a;
			Cercle d = (Cercle) b;
			int cx = (int) c.x + c.getRayon();
			int cy = (int) c.y + c.getRayon();
			int dx = (int) d.x + d.getRayon();
			int dy = (int) d.y + d.getRayon();			
			int distancecarre = (cx - dx)*(cx - dx) + (cy - dy)*(cy - dy);
			if (distancecarre < (c.getRayon()+d.getRayon())*(c.getRayon()+d.getRayon()))
				resultat = true;			
			else resultat = false;	
		}

		if (a.type == 1 && b.type == 2){
			Figure c = a;
			a = b;
			b = c;
		}

		if (a.type == 2 && b.type == 1){
			Rectangle c = (Rectangle) b;
			Cercle d = (Cercle) a;

			int xcentrecercle = (int) d.x + d.getRayon();
			int ycentrecercle = (int) d.y + d.getRayon();
			int xcentrerect = (int) c.x + c.longu/2;
			int ycentrerect = (int) c.y + c.largu/2;
			if (c.dedans(xcentrecercle, ycentrecercle) || d.dedans(xcentrerect, ycentrerect)) 
				resultat = true;/*cercle dans rectangle ou rectangle dans cercle*/
			else if (xcentrecercle < c.x){
				/*haut gauche ou milieu gauche ou bas gauche*/
				if (ycentrecercle < c.y){
					/*haut gauche*/
					if ((xcentrecercle - c.x)*(xcentrecercle - c.x) + (ycentrecercle - c.y)*(ycentrecercle - c.y) <= d.getRayon()*d.getRayon()){
						resultat = true;						
					}					
					else resultat = false;
				}
				else if (ycentrecercle > c.y+c.getHauteur()){
					/*bas gauche*/
					int yy = (int) c.y+c.getHauteur();
					if ((xcentrecercle - c.x)*(xcentrecercle - c.x) + (ycentrecercle - yy)*(ycentrecercle - yy) <= d.getRayon()*d.getRayon()){
						resultat = true;
					}
					else resultat = false;
				}
				/*milieu gauche*/
				else if (c.dedans(xcentrecercle+d.getRayon(),ycentrecercle)){
					resultat = true;
				}
				else resultat = false;
			}

			else if (xcentrecercle > c.x+c.getLargeur()){
				int xx = (int) c.x + c.getLargeur();
				/*haut droite ou milieu droite ou bas droite*/
				if (ycentrecercle < c.y){
					/*haut droite*/
					if ((xcentrecercle - xx)*(xcentrecercle - xx) + (ycentrecercle - c.y)*(ycentrecercle - c.y) <= d.getRayon()*d.getRayon()){
						resultat = true;
					}
					else resultat = false;
				}
				else if (ycentrecercle > c.y+c.getHauteur()){
					/*bas droite*/
					int yy = (int) c.y+c.getHauteur();
					if ((xcentrecercle - xx)*(xcentrecercle - xx) + (ycentrecercle - yy)*(ycentrecercle - yy) <= d.getRayon()*d.getRayon()){
						resultat = true;
					}
					else resultat = false;
				}
				/*milieu droite*/
				else if (c.dedans(xcentrecercle-d.getRayon(),ycentrecercle)){
					resultat = true;
				}
				else resultat = false;
			}
			else if (ycentrecercle < c.y){
				/*milieu haut*/
				if (c.dedans(xcentrecercle,ycentrecercle+d.getRayon())){
					resultat = true;
				}
				else resultat = false;
			}
			/*milieu bas*/

			else if (c.dedans(xcentrecercle,ycentrecercle-d.getRayon())){
				resultat = true;
			}
			else resultat = false;
		}

		/* on sait maintenant si les figures peuvent interagir mais en ont elle le droit ?*/
		if ((a.interdiction.contains(b)) && resultat){
			return false;
		}
		else if ((a.interdiction.contains(b)) && (!(resultat))){
			/* retablissement de l'interaction*/
			a.interdiction.removeElement(b);
			b.interdiction.removeElement(a);
			return false;
		}
		else if (resultat){
			a.interdiction.add(b);
			b.interdiction.add(a);
		}
		return resultat;
	}


}
