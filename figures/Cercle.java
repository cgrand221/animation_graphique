package figures;

import java.awt.Color;
import java.awt.Graphics;

public class Cercle extends Figure{
	private int rayon;

	public Cercle(int x, int y, Color c, int rayon,int xv,int yv,boolean plein) {
		super(x, y, c,xv,yv,plein);
		this.rayon = rayon;
		this.type = 2;
		this.longu = rayon*2;
		this.largu = rayon*2;
	}
	
	public int getRayon() {
		return rayon;
	}

	public void dessin (Graphics g) {

		if (this.plein){
			/*on donne l'apparence a de veritable boules */
			int r = this.couleur.getRed();
			int v = this.couleur.getGreen();
			int b = this.couleur.getBlue();
			int ri = r; /* rouge intermediaire*/
			int vi = v; /* vert intermediaire*/
			int bi = b; /* bleu intermediaire*/
			int ra = (255 - r) / this.rayon ; /*coef directeur couleur r*/
			int va = (255 - v) / this.rayon ; /*coef directeur couleur v*/
			int ba = (255 - b) / this.rayon ; /*coef directeur couleur b*/
			
			for (int j=0;j<this.rayon;j++){
				/*
				 * calcul de la couleur de this.couleur jusqua blanc:
				 * principe:
				 * soit f la fonction qui calcule la couleur
				 * f(0) = c    => lorsque le rayon est normal la couleur est normale
				 * f(this.rayon) = 255   => lorsque le rayon est nul la couleur est blanche
				 * f(x) = ax + b
				 * a = (255 -c) / (this.rayon - 0)
				 * b = f(0) = c
				 * pour simplifier les calculs
				 * on a:
				 * f(j+1) = a(j+1) + b = aj + b + a = f(j) + a
				 * il suffira donc a chaque tour de boucle de prendre le resultat
				 * de la couleur precedente et d'aujouter le codef directeur 
				 */
				g.setColor(new Color(ri,vi,bi));				
				g.fillOval((int) (x+j),((int) y)+j, 2*(this.rayon-j),2*(this.rayon-j));
				ri = ri + ra;
				vi = vi + va;
				bi = bi + ba;				
			}
		}
		else {
			g.setColor(this.couleur);
			g.drawOval((int) x,(int) y, 2*this.rayon,2*this.rayon);			
		}
	}	
	public boolean dedans (int x, int y) {
		
		int xcentre = (int) this.x+this.rayon;
		int ycentre = (int) this.y+this.rayon;
		return ((x-xcentre)*(x-xcentre)+(y-ycentre)*(y-ycentre) <= this.rayon*this.rayon);
	}
}
