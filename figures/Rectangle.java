package figures;

import java.awt.*;

public class Rectangle extends Figure{
	private int largeur;
	private int hauteur;
	
	public Rectangle(int x,int y,int largeur,int hauteur,Color c,int xv,int yv,boolean plein){
		super(x,y,c,xv,yv,plein);
		this.type=1;
		this.hauteur = hauteur;
		this.largeur = largeur;
		this.longu = largeur;
		this.largu = hauteur;

	}
	public int getLargeur() {
		return largeur;
	}
	public int getHauteur() {
		return hauteur;
	}
	public void dessin (Graphics g) {
		g.setColor(this.couleur);
		if (this.plein){
			/*on donne l'apparence a des rectangles pleins axpos�s a la lumiere */
			/*pour le calcul des couleurs voir raisonnement fait pour le cercle*/
			int r = this.couleur.getRed();
			int v = this.couleur.getGreen();
			int b = this.couleur.getBlue();
			float ri = r; /* rouge intermediaire*/
			float vi = v; /* vert intermediaire*/
			float bi = b; /* bleu intermediaire*/
			float ra = ((float) (255 - r)) / (this.largeur/2); /*coef directeur couleur r*/
			float va = ((float) (255 - v)) / (this.largeur/2); /*coef directeur couleur v*/
			float ba = ((float) (255 - b)) / (this.largeur/2); /*coef directeur couleur b*/
			
			double xx = this.x;
			double yy = this.y;
			int llarg = this.largeur;
			double hhaut = this.hauteur;
			
			double aa = (float) this.hauteur / this.largeur;
			
			for (int j=0;j<this.largeur/2;j++){	
				/* calcul des coordonn�es pour chaque rectangles
				 * soit g la fonction qui a une ordonnee y associe une nouvelle ordonnee
				 * (les abcisses sont faciles a calculer : elles augmentent de 1 a chaque fois !)
				 * g(0) = this.hauteur
				 * g(this.largeur) = 0
				 * coef directeur aa = - this.hauteur/this.largeur
				 * g(y+1) = aa(y+1) + this.hauteur = aa*y + this.hauteur + aa = g(y) + aa
				 * => xx = xx + 1      (abscisse du nouveau rectangle)
				 * => yy = yy + aa
				 * => llarg = llarg - 2 (la largeur diminue 2 fois plus vite car il faut laisser
				 *     l'espace a droite et a gauche)
				 * =>  hhaut = hhaut - 2*aa     (la hauteur diminue 2 fois plus vite car il faut laisser
				 *     l'espace a droite et a gauche mais diminue de la meme facon que yy)
				 */
				g.setColor(new Color((int) ri,(int) vi,(int) bi));				
				g.fillRect((int) xx,(int) yy, llarg,(int) hhaut);
				ri = ri + ra;
				vi = vi + va;
				bi = bi + ba;
				xx = xx + 1;
				yy = yy + aa;
				llarg = llarg - 2;
				hhaut = hhaut - 2 * aa;				
			}
		}
		else {
			g.setColor(this.couleur);
			g.drawRect((int) x,(int) y, this.largeur,this.hauteur);			
		}	
	}	
	public boolean dedans (int x, int y) {
		
		return ((x>=this.x) && (x<=this.x+this.largeur) && (y>=this.y) && (y<=this.y+this.hauteur));

	}
	
}
