package figures;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.*;
import javax.swing.JPanel;
import java.util.Vector;
import javax.swing.*;

public class DessinFigures extends JPanel implements MouseListener,ActionListener {
	
	

	private static final long serialVersionUID = 1L;
	private Vector figs;
	Timer t;
	boolean anime;
	boolean interaction;
	double gravitation;
	Figure pasCalcul;
	public DessinFigures(Vector v,boolean anime,boolean interaction){
		super();
		this.pasCalcul = null;
		this.t=new Timer(10,this);
		this.figs = v;
		this.setPreferredSize(new Dimension(700,500));
		this.addMouseListener(this);
		this.anime = anime;
		this.interaction = interaction;
		this.t.start();
		this.t.stop();/*l'animation au depart est arret�e*/
		this.gravitation = 0;
	}
	
	public void stopperTimer(){
		this.t = null;
	}
	
	public void suspendreTimer(){
		if (this.anime)/* 
		 				* suspension de l'execution lorsque la fenetre est desactivee :
		 				* si animation alors desactiver animation sinon ne rien faire :
		 				* l'animation est deja desactivee 
		 				*/
			this.t.stop();
	}
	
	public void reprendreTimer(){  /* 
	 								* reprise de l'execution lorsque la fenetre est reactivee :
	 								* si animation alors sactiver animation sinon ne rien faire :
	 								* l'animation �tait deja desactivee avant de desactiver la fenetre
	 								*/
		if (this.anime)
			this.t.restart();
	}

	public void paintComponent(Graphics g){
		super.paintComponent(g);
		Figure figAti = null;
		if (this.anime)
			this.figs = ((new trajectoires(this.figs,this.interaction,this.gravitation)).getV());
		for (int i=0;i<this.figs.size();i++){
			figAti = (Figure) this.figs.elementAt(i);
			if ((this.anime) && (this.pasCalcul != figAti)){
				figAti.yvitesse += this.gravitation;
				figAti.x += figAti.xvitesse;
				figAti.y += figAti.yvitesse;
			}
			((Figure) this.figs.elementAt(i)).dessin(g);	
		}
		/* dessin du cadre */
		g.setColor(Color.black);
		g.drawRect(0, 0, 699, 499);		
	}
	


	@Override
	public void mouseClicked(MouseEvent e) {
		int i=this.figs.size()-1;
		while ((i>=0) && (!((Figure) this.figs.elementAt(i)).dedans(e.getX(), e.getY()))){
			i--;	
		}
		if (i != -1){
			this.figs.remove(i);
			this.repaint();
		}	
	}



	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		this.repaint();
		// TODO Auto-generated method stub
		
	}
	
}
