package figures;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Date;
import java.util.Vector;

import javax.swing.*;


public class SaisieFigure extends JPanel implements ActionListener,MouseMotionListener,MouseListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton couleur;
	private JButton rectangle;
	private JButton cercle;
	private JButton quitter;
	private JButton animation;
	private JButton reset;
	private JButton interagir;
	private JButton plein;
	private boolean remplissage;
	private boolean interaction;
	private JPanel panneauBoutons;
	private JPanel panneauSaisie;
	private DessinFigures dessinFigures;
	private JLabel abs;
	private JTextField abscisse;
	private boolean anime;
	Vector figs;
	JButton about;
	private JLabel ord;
	private JTextField ordonnee;

	private JLabel larg;
	private JTextField largeur;

	private JLabel hau;
	private JTextField hauteur;

	private JLabel ray;
	private JTextField rayon;

	private JLabel vitx;
	private JTextField vitessex;

	private JLabel vity;
	private JTextField vitessey;


	private boolean debutDeplacement;
	Figure figureADeplacer;
	int dx,dy;
	int debutx,debuty,temps;
	Date date;
	JLabel grav;
	JTextField gravi;
	JButton confirmer;

	public Color getCouleur() {
		return this.couleurs[this.rotation];
	}

	Color[] couleurs;
	int rotation;

	public SaisieFigure(Vector v,DessinFigures d){
		this.rotation = 15;
		this.couleurs = new Color[16];
		this.couleurs[0] = new Color(255,00,00);
		this.couleurs[1] = new Color(00,255,00);
		this.couleurs[2] = new Color(00,00,255);
		this.couleurs[3] = new Color(255,255,00);
		this.couleurs[4] = new Color(255,00,255);
		this.couleurs[5] = new Color(00,255,255);
		this.couleurs[6] = new Color(255,255,255);
		this.couleurs[7] = new Color(00,00,00);
		this.couleurs[8] = new Color(127,00,00);
		this.couleurs[9] = new Color(00,127,00);
		this.couleurs[10] = new Color(00,00,127);
		this.couleurs[11] = new Color(127,127,00);
		this.couleurs[12] = new Color(127,00,127);
		this.couleurs[13] = new Color(00,127,127);
		this.couleurs[14] = new Color(127,127,127);
		this.couleurs[15] = new Color(00,00,00);
		this.debutDeplacement = false;
		this.figs = v;
		this.couleur = new JButton("couleur");
		this.cercle = new JButton("cercle");
		this.rectangle = new JButton("rectangle");
		this.about = new JButton("� propos");
		this.about.addActionListener(this);
		this.quitter = new JButton("quitter");
		this.animation = new JButton("animation : non anim�");
		this.interagir = new JButton("interraction : non");
		this.interaction = false;
		this.reset = new JButton("Effacer l'�cran");
		this.anime = false;
		this.plein = new JButton("remplissage figures : non");
		this.remplissage = false;

		this.abs = new JLabel("abscisse");
		this.abscisse = new JTextField("50",5);
		this.ord = new JLabel("ordonnee");
		this.ordonnee = new JTextField("50",5);
		this.larg = new JLabel("largeur");
		this.largeur = new JTextField("50",5);
		this.hau = new JLabel("hauteur");
		this.hauteur = new JTextField("30",5);
		this.ray = new JLabel("rayon");
		this.rayon = new JTextField("70",5);
		this.vitx = new JLabel("vistesse abscisse");
		this.vitessex = new JTextField("1",5);
		this.vity = new JLabel("vitesse ordonnee");
		this.vitessey = new JTextField("3",5);
		this.grav = new JLabel("gravitation");
		this.gravi = new JTextField("0",5);
		this.confirmer = new JButton("confirmer");
		this.confirmer.addActionListener(this);
		this.panneauSaisie = new JPanel();
		this.dessinFigures = d;
		this.panneauBoutons = new JPanel();	
		this.dessinFigures.setPreferredSize(new Dimension(700,500));
		this.dessinFigures.addMouseMotionListener(this);
		this.dessinFigures.addMouseListener(this);

		this.couleur.addActionListener(this);
		this.cercle.addActionListener(this);
		this.animation.addActionListener(this);
		this.rectangle.addActionListener(this);
		this.quitter.addActionListener(this);
		this.reset.addActionListener(this);
		this.interagir.addActionListener(this);
		this.plein.addActionListener(this);

		this.panneauBoutons.add(this.rectangle);
		this.panneauBoutons.add(this.cercle);
		this.panneauBoutons.add(this.animation);
		this.panneauBoutons.add(this.reset);
		this.panneauBoutons.add(this.interagir);
		this.panneauBoutons.add(this.plein);
		this.panneauSaisie.add(this.abs);
		this.panneauSaisie.add(this.abscisse);
		this.panneauSaisie.add(this.ord);
		this.panneauSaisie.add(this.ordonnee);
		this.panneauSaisie.add(this.larg);
		this.panneauSaisie.add(this.largeur);
		this.panneauSaisie.add(this.hau);
		this.panneauSaisie.add(this.hauteur);
		this.panneauSaisie.add(this.ray);
		this.panneauSaisie.add(this.rayon);
		this.panneauSaisie.add(this.vitx);
		this.panneauSaisie.add(this.vitessex);
		this.panneauSaisie.add(this.vity);
		this.panneauSaisie.add(this.vitessey);
		this.figureADeplacer = null;
		this.panneauSaisie.add(this.grav);
		this.panneauSaisie.add(this.gravi);
		this.panneauSaisie.add(this.confirmer);


		/*this.panneauSaisie.setPreferredSize(new Dimension(145,300));*/
		this.panneauSaisie.setLayout(new GridLayout(14,1));
		this.add(this.panneauSaisie);
		this.add(this.dessinFigures);
		this.add(this.panneauBoutons);

		this.add(this.couleur);
		this.add(this.about);
		this.add(this.quitter);

		this.dx = 0;
		this.dy = 0;
		this.debutx = 0;
		this.debuty = 0;
		this.temps = 0;
		this.date = null;

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.couleur){
			this.rotation = (this.rotation + 1) % this.couleurs.length;
			this.couleur.setBackground(this.couleurs[this.rotation]);
			/*System.out.println("couleur");*/
		}
		if (e.getSource() == this.rectangle){
			try{
				int a = Integer.parseInt((this.abscisse.getText()));
				int o = Integer.parseInt((this.ordonnee.getText()));
				int l = Integer.parseInt((this.largeur.getText()));
				int h = Integer.parseInt((this.hauteur.getText()));
				int vx = Integer.parseInt((this.vitessex.getText()));
				int vy = Integer.parseInt((this.vitessey.getText()));
				if ((a>=0) && (o>= 0) && (l>1) && (h>1)){/*1/2 = 0*/
					Rectangle r = new Rectangle(a,o,l,h,this.couleurs[this.rotation],vx,vy,this.remplissage);
					this.figs.add(r);
					this.dessinFigures.repaint();
				}
				else JOptionPane.showMessageDialog(this,"veillez entrez les informations correctement !", "erreur", JOptionPane.ERROR_MESSAGE);
			}
			catch (Exception exc){
				JOptionPane.showMessageDialog(this,"veillez entrez les informations correctement !", "erreur", JOptionPane.ERROR_MESSAGE);
			}

		}
		if (e.getSource() == this.cercle){
			try{
				int a = Integer.parseInt((this.abscisse.getText()));
				int o = Integer.parseInt((this.ordonnee.getText()));
				int r = Integer.parseInt((this.rayon.getText()));
				int vx = Integer.parseInt((this.vitessex.getText()));
				int vy = Integer.parseInt((this.vitessey.getText()));
				if ((a>=0) && (o>=0) && (r>1)){/*1/2 = 0*/
					Cercle c = new Cercle(a,o,this.couleurs[this.rotation],r,vx,vy,this.remplissage);
					this.figs.add(c);
					this.dessinFigures.repaint();
				}
				else JOptionPane.showMessageDialog(this,"veillez entrez les informations correctement !", "erreur", JOptionPane.ERROR_MESSAGE);
			}
			catch (Exception exc){
				JOptionPane.showMessageDialog(this,"veillez entrez les informations correctement !", "erreur", JOptionPane.ERROR_MESSAGE);
			}

		}
		if (e.getSource() == this.quitter){
			int reponse = JOptionPane.showConfirmDialog(this, "Etes-vous s�r de vouloir quitter ?", "confirmation", JOptionPane.YES_NO_CANCEL_OPTION);
			if (reponse == JOptionPane.YES_OPTION){
				this.dessinFigures.t = null;
				System.exit(0);
			}
		}	
		if (e.getSource() == this.animation){
			if (!(this.anime)){
				this.animation.setText("animation : en cours");
				this.anime = true;
				this.dessinFigures.anime = true;
				this.dessinFigures.t.restart();
			}
			else {
				this.animation.setText("animation : non anim�");
				this.anime = false;
				this.dessinFigures.anime = false;
				this.dessinFigures.t.stop();
			}
		}
		if (e.getSource() == this.reset){
			this.figs.removeAllElements();
			this.repaint();

		}
		if (e.getSource() == this.interagir){
			if (this.interaction){
				this.interaction = false;
				this.interagir.setText("interraction : non");
				this.interagir.setBackground(Color.WHITE);
				this.dessinFigures.interaction = false;
			}
			else {
				this.interaction = true;
				this.interagir.setText("interraction : oui");
				this.interagir.setBackground(Color.RED);
				this.dessinFigures.interaction = true;
			}

		}
		if (e.getSource() == this.plein){
			if (this.remplissage){
				this.remplissage = false;
				this.plein.setText("remplissage figures : non");
				this.cercle.setText("cercle");
				this.rectangle.setText("rectangle");
			}
			else {
				this.remplissage = true;
				this.plein.setText("remplissage figures : oui");				
				this.cercle.setText("sph�re");
				this.rectangle.setText("pav�");

			}

		}
		if (e.getSource() == this.confirmer){
			try{
				dessinFigures.gravitation = ((double)Integer.parseInt((this.gravi.getText())))/100;				
			}
			catch (Exception exc){
				JOptionPane.showMessageDialog(this,"veillez entrez les informations correctement !", "erreur", JOptionPane.ERROR_MESSAGE);
			}
		}
		if (e.getSource() == this.about){
			JOptionPane.showMessageDialog(this,"Application cr�e par Christopher GRAND","A propos",JOptionPane.INFORMATION_MESSAGE);
		}

	}

	@Override
	public void mouseDragged(MouseEvent e) {

		if (!(this.debutDeplacement)){

			int i=this.figs.size()-1;
			this.debutDeplacement = true;/*on met en variable d'instance la figure que 
			 * l'on deplace : apres plus besoins de rechercher la figure a deplacer
			 */

			/*
			 * lors de l'affichage on affiche du debut a la fin du vecteur, donc
			 * lorsqu'on clique sur une figure on choisi la figure a partir de la fin du vecteur
			 * on parcours donc le vecteur a l'envers
			 */
			while ((i>=0) && !((Figure) this.figs.elementAt(i)).dedans(e.getX(), e.getY())){
				i--;
			}
			if (i>=0){
				this.figureADeplacer = (Figure) this.figs.elementAt(i);
				this.dx = (int) this.figureADeplacer.x - e.getX();
				this.dy = (int) this.figureADeplacer.y - e.getY();
				this.figureADeplacer.xvitesse = 0;
				this.figureADeplacer.yvitesse = 0;
				this.debutx = e.getX();
				this.debuty = e.getY();
				this.temps = 0;
				this.date = new Date();
				this.dessinFigures.pasCalcul = this.figureADeplacer;



			}
			else {
				this.figureADeplacer = null;
			}				
		}
		else {

			if (this.figureADeplacer != null){
				this.figureADeplacer.x = e.getX() + this.dx;
				this.figureADeplacer.y = e.getY() + this.dy;
				this.temps += 1;/*en 10*1ms*/
				this.date = new Date();
				this.dessinFigures.repaint();				
			}
		}



	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		Date t = new Date();
		if ((this.figureADeplacer != null) && (this.temps !=0)){
			this.dessinFigures.pasCalcul = null;
			double delai = (t.getTime() - this.date.getTime());
			if (delai>15){	/* test immobilite = temps d'attente long compar� a 10 ms 
			 * car lorsquon reste sur place le temps passe mais on ne passe pas 
			 * par cette fonction
			 */
				this.figureADeplacer.xvitesse=0;
				this.figureADeplacer.yvitesse=0;
				this.figureADeplacer = null;
			}
			else {
				this.figureADeplacer.xvitesse=(e.getX()-this.debutx)/this.temps;
				this.figureADeplacer.yvitesse=(e.getY()-this.debuty)/this.temps;
				this.figureADeplacer = null;
			}
		}
		this.debutDeplacement = false;
		// TODO Auto-generated method stub

	}


}
