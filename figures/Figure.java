/* Figure.java */
package figures;
import java.awt.*;
import java.util.Vector;
public class Figure {
	// Variables d�instance
	protected int longu,largu; // longueur/largeur de la figure
	double x,y;/*coordonnees du coin superieur gauche de la figure : x,y flotant (pour la gravitation qui utilise des flotants)*/
	protected int type;
	protected double xvitesse,yvitesse;
	protected boolean plein;
	protected Color couleur; // couleur de la figure
	protected Vector interdiction;// liste de figures pour lequel cette figure ne foit pas interagir
	// Constructeur
	public Figure(int x, double y, Color c,int xv,int yv,boolean plein) {
		this.x = x;
		this.y = y;
		this.couleur = c;
		this.xvitesse = xv;
		this.yvitesse = yv;
		this.longu = 0;
		this.largu = 0;
		this.plein = plein;
		this.interdiction = new Vector();
	}
	// Methodes d�instance ( a redefinir dans les sous-classes )
	public void dessin (Graphics g) {
	}	
	public boolean dedans (int x, int y) {
		return false;
	}
}
