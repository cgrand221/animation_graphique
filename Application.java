import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import figures.DessinFigures;
import figures.EcouteurFenetre;
import figures.SaisieFigure;
import java.util.*;

public class Application extends JPanel implements WindowListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1423645944013554378L;
	public static DessinFigures d;
	public static void main(String[] args){
		JFrame f=new JFrame("Animation");
		Vector v = new Vector();
		DessinFigures d = new DessinFigures(v,false,false);
		Application.d = d;
		f.add(new SaisieFigure(v,d));
		f.setSize(new Dimension(1000,700));
		f.addWindowListener(new EcouteurFenetre(f,Application.d));
		f.setVisible(true);		
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		
		
	}


	public void windowClosing(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}	
}
